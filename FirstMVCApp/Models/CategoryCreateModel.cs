﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Models
{
    public class CategoryCreateModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name cannot be empty")]
        public string Name { get; set; }
    }
}
