﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models
{
    public class CategoryFilterModel
    {
        public string Name { get; set; }
    }
}
