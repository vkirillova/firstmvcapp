﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Models
{
    public class ProductCreateModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name cannot be empty")]
        public string Name { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category cannot be empty")]
        public int CategoryId { get; set; }

        [Display(Name = "Brand")]
        public int? BrandId { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "Price cannot be empty")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Price cannot be negative")]
        public decimal Price { get; set; }
        public SelectList CategoriesSelect { get; set; }
        public SelectList BrandsSelect { get; set; }
    }
}
