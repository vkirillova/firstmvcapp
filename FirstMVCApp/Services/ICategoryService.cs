﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.Models;

namespace FirstMVCApp.Services
{
    public interface ICategoryService
    {
        List<CategoryModel> Categories(CategoryFilterModel model);
        CategoryCreateModel GetCategoryCreateModel();
        void CreateCategory(CategoryCreateModel model);
    }
}
