﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.Models;

namespace FirstMVCApp.Services
{
    public interface IBrandService
    {
        List<BrandModel> Brands(BrandFilterModel model);
        BrandCreateModel GetBrandCreateModel();
        void CreateBrand(BrandCreateModel model);
    }
}
