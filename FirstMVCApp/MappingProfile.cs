﻿using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;
using System;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();

            CreateCategoryToCategoryModelMap();
            CreateCategoryCreateModelToCategory();

            CreateBrandToBrandModelMap();
            CreateBrandCreateModelToBrand();
        }

        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }

        private void CreateBrandToBrandModelMap()
        {
            CreateMap<Brand, BrandModel>();
        }

        private void CreateCategoryCreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }

        private void CreateCategoryToCategoryModelMap()
        {
            CreateMap<Category, CategoryModel>();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.Brand,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.Category,
                    src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }
    }
}
