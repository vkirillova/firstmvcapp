﻿using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories
{
    public class BrandRepository : Repository<Brand>, IBrandRepository
    {
        public BrandRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Brands;
        }
    }
}