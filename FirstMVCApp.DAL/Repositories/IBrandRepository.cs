﻿using System.Collections.Generic;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories
{
    public interface IBrandRepository : IRepository<Brand>
    {
    }
}
