﻿using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
